package ru.bokhan.tm.api;

import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.endpoint.SessionDTO;

public interface ICurrentSessionService {

    void setCurrentSession(@Nullable final SessionDTO currentSession);

    @Nullable SessionDTO getCurrentSession();

}
