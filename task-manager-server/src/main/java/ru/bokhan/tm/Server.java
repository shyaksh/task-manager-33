package ru.bokhan.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.bokhan.tm.bootstrap.Bootstrap;
import ru.bokhan.tm.configuration.ServerConfiguration;

public class Server {

    public static void main(@Nullable final String[] args) {
        @NotNull final AnnotationConfigApplicationContext context =
                new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        context.registerShutdownHook();
        bootstrap.run(args);
    }

}