package ru.bokhan.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    //void init();

    @NotNull String getServerHost();

    @NotNull Integer getServerPort();

    @NotNull String getSessionSalt();

    @NotNull Integer getSessionCycle();

    @NotNull String getJdbcDriver();

    @NotNull String getJdbcUrl();

    @NotNull String getJdbcLogin();

    @NotNull String getJdbcPassword();

}