package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.bokhan.tm.api.repository.IUserRepository;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.entity.User;

import java.util.List;

@Repository
public final class UserRepository extends AbstractRepository<UserDTO> implements IUserRepository {

    @Nullable
    @Override
    public UserDTO findByLogin(@NotNull final String login) {
        @NotNull final List<UserDTO> resultList = em.createQuery("SELECT e FROM UserDTO e WHERE e.login = :login", UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    @Override
    public void removeById(@NotNull final String id) {
        @Nullable final UserDTO user = findById(id);
        if (user == null) return;
        remove(user);
    }

    @Override
    public void removeByLogin(@NotNull final String login) {
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return;
        remove(user);
    }

    @Override
    @NotNull
    public List<UserDTO> findAll() {
        return em.createQuery("SELECT e FROM UserDTO e", UserDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public UserDTO findById(@NotNull String id) {
        @NotNull final List<UserDTO> resultList = em.createQuery("SELECT e FROM UserDTO e WHERE e.id = :id", UserDTO.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    @Override
    public long count() {
        @NotNull final List<Long> resultList = em.createQuery("SELECT COUNT(e) FROM UserDTO e", Long.class).setMaxResults(1)
                .getResultList();
        return resultList.isEmpty() ? 0 : resultList.get(0);
    }


    @Override
    public void remove(@NotNull UserDTO userDTO) {
        @NotNull final List<User> resultList = em.createQuery("SELECT e FROM User e WHERE e.id = :id", User.class)
                .setParameter("id", userDTO.getId())
                .setMaxResults(1)
                .getResultList();
        if (resultList.isEmpty()) return;
        em.remove(resultList.get(0));
    }

}

