package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.bokhan.tm.api.repository.ISessionRepository;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.entity.Session;

import java.util.List;

@Repository
public final class SessionRepository extends AbstractRepository<SessionDTO> implements ISessionRepository {

    @NotNull
    @Override
    public List<SessionDTO> findByUserId(@NotNull final String userId) {
        return em.createQuery("SELECT e FROM SessionDTO e WHERE e.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Override
    public void removeByUserId(@NotNull final String userId) {
        em.createQuery("DELETE FROM SessionDTO e WHERE e.userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    @NotNull
    public List<SessionDTO> findAll() {
        return em.createQuery("SELECT e FROM SessionDTO e", SessionDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findById(@NotNull String id) {
        @NotNull final List<SessionDTO> resultList = em.createQuery(
                "SELECT e FROM SessionDTO e WHERE e.id = :id", SessionDTO.class
        )
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    @Override
    public long count() {
        @NotNull final List<Long> resultList = em.createQuery(
                "SELECT COUNT(e) FROM SessionDTO e", Long.class
        )
                .setMaxResults(1)
                .getResultList();
        return resultList.isEmpty() ? 0 : resultList.get(0);
    }

    @Override
    public void remove(@NotNull final SessionDTO dto) {
        @NotNull final List<Session> resultList = em.createQuery(
                "SELECT e FROM Session e WHERE e.id = :id", Session.class
        )
                .setParameter("id", dto.getId())
                .setMaxResults(1)
                .getResultList();
        if (resultList.isEmpty()) return;
        em.remove(resultList.get(0));
    }

}
