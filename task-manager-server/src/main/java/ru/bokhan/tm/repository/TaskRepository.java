package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.bokhan.tm.api.repository.ITaskRepository;
import ru.bokhan.tm.dto.TaskDTO;
import ru.bokhan.tm.entity.Task;

import java.util.List;

@Repository
public final class TaskRepository extends AbstractRepository<TaskDTO> implements ITaskRepository {

    @Override
    public @NotNull List<TaskDTO> findAll() {
        return em.createQuery("SELECT e FROM TaskDTO e", TaskDTO.class)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findById(@NotNull String id) {
        @NotNull final List<TaskDTO> resultList = em.createQuery(
                "SELECT e FROM TaskDTO e WHERE e.id = :id", TaskDTO.class
        )
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    @Override
    public long count() {
        @NotNull final List<Long> resultList = em.createQuery(
                "SELECT COUNT(e) FROM TaskDTO e", Long.class
        ).setMaxResults(1)
                .getResultList();
        return resultList.isEmpty() ? 0 : resultList.get(0);
    }


    @Override
    public void clear(@NotNull final String userId) {
        em.createQuery("DELETE FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll(@NotNull final String userId) {
        return em.createQuery("SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final List<TaskDTO> resultList = em.createQuery(
                "SELECT e FROM TaskDTO e WHERE e.id = :id AND e.userId = :userId", TaskDTO.class
        )
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    @Nullable
    @Override
    public TaskDTO findByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final List<TaskDTO> list = em.createQuery(
                "SELECT e FROM TaskDTO e WHERE e.userId = :userId", TaskDTO.class
        )
                .setParameter("userId", userId)
                .getResultList();
        if (index > list.size()) return null;
        return list.get(index);
    }

    @Nullable
    @Override
    public TaskDTO findByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final List<TaskDTO> resultList = em.createQuery(
                "SELECT e FROM TaskDTO e WHERE e.userId = :userId AND e.name = :name", TaskDTO.class
        )
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getResultList();
        return resultList.isEmpty() ? null : resultList.get(0);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final TaskDTO task = findById(userId, id);
        if (task == null) return;
        remove(task);
    }

    @Override
    public void removeByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @Nullable final TaskDTO task = findByIndex(userId, index);
        if (task == null) return;
        remove(task);
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final TaskDTO task = findByName(userId, name);
        if (task == null) return;
        remove(task);
    }

    @Override
    public void remove(@NotNull final TaskDTO dto) {
        @NotNull final List<Task> resultList = em.createQuery(
                "SELECT e FROM Task e WHERE e.id = :id", Task.class
        )
                .setParameter("id", dto.getId())
                .setMaxResults(1)
                .getResultList();
        if (resultList.isEmpty()) return;
        em.remove(resultList.get(0));
    }

}