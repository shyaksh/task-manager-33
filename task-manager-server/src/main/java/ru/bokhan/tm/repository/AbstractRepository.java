package ru.bokhan.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.bokhan.tm.api.IRepository;
import ru.bokhan.tm.dto.AbstractEntityDTO;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntityDTO> implements IRepository<E> {

    @NotNull
    @PersistenceContext
    protected EntityManager em;

    @Override
    public void merge(@NotNull final E e) {
        em.merge(e);
    }

    @Override
    public void persist(@NotNull final E e) {
        em.persist(e);
    }

    @Override
    public void mergeAll(@NotNull final List<E> list) {
        list.forEach(e -> em.merge(e));
    }

    @Override
    public void load(@NotNull final List<E> list) {
        clear();
        mergeAll(list);
    }

    @Override
    public boolean contains(@NotNull E e) {
        return em.contains(e);
    }

    @Override
    public void clear() {
        findAll().forEach(em::remove);
    }

}
