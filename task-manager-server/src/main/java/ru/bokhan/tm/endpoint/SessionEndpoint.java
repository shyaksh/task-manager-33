package ru.bokhan.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Controller;
import ru.bokhan.tm.api.endpoint.ISessionEndpoint;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.empty.EmptyLoginException;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
@NoArgsConstructor
public final class SessionEndpoint extends AbstractEndpoint implements ISessionEndpoint {

    @Override
    @WebMethod
    public void closeSession(
            @WebParam(name = "session") @Nullable SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        sessionService.close(session);
    }

    @Override
    @WebMethod
    public void closeSessionAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        sessionService.closeAll(session);
    }

    @Override
    @Nullable
    @WebMethod
    public UserDTO getUserBySession(
            @WebParam(name = "session") @Nullable SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return sessionService.getUser(session);
    }

    @Override
    @NotNull
    @WebMethod
    public String getUserIdBySession(
            @WebParam(name = "session") @Nullable SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return sessionService.getUserId(session);
    }

    @Override
    @NotNull
    @WebMethod
    public List<SessionDTO> findSessionAll(
            @WebParam(name = "session") @Nullable SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        return sessionService.findAll(session);
    }

    @Override
    @Nullable
    @WebMethod
    public SessionDTO openSession(
            @WebParam(name = "login") @Nullable String login,
            @WebParam(name = "password") @Nullable String password
    ) {
        if (login == null) throw new EmptyLoginException();
        if (password == null) throw new EmptyLoginException();
        return sessionService.open(login, password);
    }

    @Override
    @WebMethod
    public void signOutByLogin(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "login") @Nullable String login
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        sessionService.signOutByLogin(login);
    }

    @Override
    @WebMethod
    public void signOutByUserId(
            @WebParam(name = "session") @Nullable SessionDTO session,
            @WebParam(name = "userId") @Nullable String userId
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        sessionService.signOutByUserId(userId);
    }

    @Override
    @WebMethod
    public void removeSessionAll(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        sessionService.clear();
    }

    @Override
    @WebMethod
    public void removeSession(@WebParam(name = "session") @Nullable SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        sessionService.remove(session);
    }

}
