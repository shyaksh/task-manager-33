package ru.bokhan.tm.endpoint;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import ru.bokhan.tm.api.endpoint.IProjectEndpoint;
import ru.bokhan.tm.api.service.IProjectService;
import ru.bokhan.tm.dto.ProjectDTO;
import ru.bokhan.tm.dto.SessionDTO;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.empty.EmptyUserIdException;
import ru.bokhan.tm.exception.security.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@Controller
@WebService
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    @NotNull
    @Autowired
    IProjectService projectService;

    @Override
    @WebMethod
    public void createProject(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name") @Nullable final String name
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        projectService.create(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createProjectWithDescription(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        projectService.create(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public void addProject(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "project") @Nullable final ProjectDTO project
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        projectService.add(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public void removeProject(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "project") @Nullable final ProjectDTO project
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        projectService.remove(session.getUserId(), project);
    }

    @Override
    @WebMethod
    public void removeProjectByUserAll(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        projectService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public void removeProjectAll(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session, Role.ADMIN);
        projectService.clear();
    }

    @Override
    @NotNull
    @WebMethod
    public List<ProjectDTO> findProjectAll(
            @WebParam(name = "session") @Nullable final SessionDTO session
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        @Nullable final String userId = session.getUserId();
        return projectService.findAll(userId);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        @Nullable final String userId = session.getUserId();
        if (userId == null) throw new EmptyUserIdException();
        return projectService.findById(userId, id);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectByIndex(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index") @Nullable final Integer index
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        return projectService.findByIndex(session.getUserId(), index);
    }

    @Override
    @Nullable
    @WebMethod
    public ProjectDTO findProjectByName(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name") @Nullable final String name
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        return projectService.findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void removeProjectById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        projectService.removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public void removeProjectByIndex(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index") @Nullable final Integer index
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        projectService.removeByIndex(session.getUserId(), index);
    }

    @Override
    @WebMethod
    public void removeProjectByName(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "name") @Nullable final String name
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        projectService.removeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "id") @Nullable final String id,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        projectService.updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectByIndex(
            @WebParam(name = "session") @Nullable final SessionDTO session,
            @WebParam(name = "index") @Nullable final Integer index,
            @WebParam(name = "name") @Nullable final String name,
            @WebParam(name = "description") @Nullable final String description
    ) {
        if (session == null) throw new AccessDeniedException();
        sessionService.validate(session);
        projectService.updateByIndex(session.getUserId(), index, name, description);
    }

}

