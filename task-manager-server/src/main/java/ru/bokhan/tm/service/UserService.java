package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.api.repository.IUserRepository;
import ru.bokhan.tm.api.service.IUserService;
import ru.bokhan.tm.dto.UserDTO;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.exception.empty.*;
import ru.bokhan.tm.exception.incorrect.IncorrectIdException;
import ru.bokhan.tm.util.HashUtil;

@Service
public class UserService extends AbstractService<UserDTO, IUserRepository> implements IUserService {

    @NotNull
    @Autowired
    private IUserRepository repository;

    @NotNull
    @Autowired
    private IUserService service;

    @NotNull
    @Override
    protected IUserRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public UserDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return repository.findByLogin(login);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(id);
    }

    @Override
    @Transactional
    public void create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        service.persist(user);
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        user.setEmail(email);
        service.persist(user);
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (role == null) throw new EmptyRoleException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        user.setRole(role);
        service.persist(user);
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String id, @Nullable final String login,
            @Nullable final String firstName, @Nullable final String lastName,
            @Nullable final String middleName,
            @Nullable final String email
    ) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (firstName == null || firstName.isEmpty()) throw new EmptyFirstNameException();
        if (middleName == null || middleName.isEmpty()) throw new EmptyMiddleNameException();
        if (lastName == null || lastName.isEmpty()) throw new EmptyLastNameException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        @Nullable final UserDTO userDTO = findById(id);
        if (userDTO == null) throw new IncorrectIdException();
        userDTO.setLogin(login);
        userDTO.setFirstName(firstName);
        userDTO.setMiddleName(middleName);
        userDTO.setLastName(lastName);
        userDTO.setEmail(email);
        service.merge(userDTO);
    }

    @Override
    @Transactional
    public void updatePasswordById(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        @Nullable final UserDTO user = findById(id);
        if (user == null) throw new IncorrectIdException();
        @Nullable final String passwordHash = HashUtil.salt(password);
        if (passwordHash == null || passwordHash.isEmpty()) throw new EmptyPasswordException();
        user.setPasswordHash(passwordHash);
        service.merge(user);
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return;
        user.setLocked(true);
        service.merge(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) return;
        user.setLocked(false);
        service.merge(user);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        repository.removeByLogin(login);
    }

}
