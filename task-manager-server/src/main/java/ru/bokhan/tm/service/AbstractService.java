package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.bokhan.tm.api.IRepository;
import ru.bokhan.tm.api.IService;
import ru.bokhan.tm.dto.AbstractEntityDTO;
import ru.bokhan.tm.exception.incorrect.IncorrectDataFileException;

import javax.transaction.Transactional;
import java.util.List;

public abstract class AbstractService<E extends AbstractEntityDTO, R extends IRepository<E>> implements IService<E> {

    @Override
    public @NotNull List<E> findAll() {
        @NotNull final R repository = getRepository();
        @NotNull final List<E> result = repository.findAll();
        return result;
    }

    @Override
    @Transactional
    public void clear() {
        @NotNull final R repository = getRepository();
        repository.clear();
    }

    @Override
    @Transactional
    public void load(@Nullable final List<E> list) {
        if (list == null) throw new IncorrectDataFileException();
        @NotNull final R repository = getRepository();
        repository.load(list);
    }

    @Override
    @Transactional
    public void remove(@Nullable final E entity) {
        if (entity == null) return;
        @NotNull final R repository = getRepository();
        repository.remove(entity);
    }

    public void merge(@NotNull E entity) {
        @NotNull final R repository = getRepository();
        repository.merge(entity);
    }

    public void persist(@NotNull E entity) {
        @NotNull final R repository = getRepository();
        repository.persist(entity);
    }

    @NotNull
    protected abstract R getRepository();

}
