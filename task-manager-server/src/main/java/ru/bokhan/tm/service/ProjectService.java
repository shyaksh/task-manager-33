package ru.bokhan.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.bokhan.tm.api.repository.IProjectRepository;
import ru.bokhan.tm.api.service.IProjectService;
import ru.bokhan.tm.dto.ProjectDTO;
import ru.bokhan.tm.exception.empty.EmptyDescriptionException;
import ru.bokhan.tm.exception.empty.EmptyIdException;
import ru.bokhan.tm.exception.empty.EmptyNameException;
import ru.bokhan.tm.exception.empty.EmptyUserIdException;
import ru.bokhan.tm.exception.incorrect.IncorrectIdException;
import ru.bokhan.tm.exception.incorrect.IncorrectIndexException;

import java.util.List;

@Service
public class ProjectService extends AbstractService<ProjectDTO, IProjectRepository> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Autowired
    private IProjectService service;

    @NotNull
    @Override
    protected IProjectRepository getRepository() {
        return repository;
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        service.persist(project);
    }

    @Override
    @Transactional
    public void create(
            @Nullable final String userId,
            @Nullable final String name,
            final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        if (description == null || description.isEmpty()) throw new EmptyDescriptionException();
        @NotNull final ProjectDTO project = new ProjectDTO();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        service.persist(project);
    }

    @Override
    @Transactional
    public void add(@Nullable final String userId, @Nullable final ProjectDTO projectDTO) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectDTO == null) throw new EmptyUserIdException();
        projectDTO.setUserId(userId);
        service.persist(projectDTO);
    }

    @Override
    @Transactional
    public void remove(@Nullable final String userId, @Nullable final ProjectDTO projectDTO) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (projectDTO == null) return;
        projectDTO.setUserId(userId);
        service.remove(projectDTO);
    }

    @NotNull
    @Override
    @Transactional(readOnly = true)
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        return repository.findAll(userId);
    }

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        repository.clear(userId);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public ProjectDTO findById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public ProjectDTO findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        return repository.findByIndex(userId, index);
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public ProjectDTO findByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByName(userId, name);
    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        repository.removeById(userId, id);
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        repository.removeByIndex(userId, index);
    }

    @Override
    @Transactional
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        repository.removeByName(userId, name);
    }

    @Override
    @Transactional
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ProjectDTO project = findById(userId, id);
        if (project == null) throw new IncorrectIdException();
        project.setName(name);
        project.setDescription(description);
        service.merge(project);
    }

    @Override
    @Transactional
    public void updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyUserIdException();
        if (index == null || index < 0) throw new IncorrectIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final ProjectDTO project = findByIndex(userId, index);
        if (project == null) throw new IncorrectIndexException();
        project.setName(name);
        project.setDescription(description);
        service.merge(project);
    }

}